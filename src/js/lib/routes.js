angular.module('routes', [
    'ui.router',
    'controller.home',
    'controller.sales',
])
    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider,$locationProvider) { //, $locationProvider
        $urlRouterProvider.otherwise("/home/");
        $stateProvider
            .state('home', {
                url: "/home/",
                views: {
                    'main@': {
                        templateUrl: "src/views/home.html",
                        controller: 'controller.home'
                    },
                }
            })
            .state('sales', {
                url: "/sales/",
                views: {
                    'main': {
                        templateUrl: "src/views/sales.html",
                        controller: 'controller.sales'
                    },
                }
            })
        // $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('');
    }]);
