'use strict';

var service = function ($q, $rootScope, $location, qlik) {
    var me = this;

    me.init = function () {
        me.version = '1.0.0'
    }

    me.boot = function () {
        me.init();
        me.events();
        me.log('App Loaded: ', me.version);
    }

    me.events = function () {
        $rootScope.goTo = (page) => {
            // api.destroyObjects().then(()=> {
            $location.url(`/${page}/`);
            // })
        }
        // Scroll Top after Route change
        $rootScope.$on('$stateChangeSuccess', function () {
            document.body.scrollTop = document.documentElement.scrollTop = 0;
            $("html, body").animate({ scrollTop: 0 }, 200);
            /*$anchorScroll('top-nav');*/
        });
        // Custom Logger
        me.log = function (type, message) {
            console.log('%c ' + type + ': ', 'color: red', message);
        };
        $rootScope.clear = (field) => {
            qlik.clear(field);
        }
    }

    return me.boot();
};
angular.module('service.app', []);
service.$inject = ['$q', '$rootScope', '$location', 'qlik'];
angular.module('service.app')
    .service('app', service);
