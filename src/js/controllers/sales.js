'use strict';

var controller = function ($scope, $rootScope, $location, $injector, qlik, api, app) {

    var me = {};

    me.init = function () {
        $rootScope.page = 'sales';
    }

    me.boot = function () {
        me.init();
        me.events();
        app.log(`${$rootScope.page} loaded: `, 'Success!');
    };

    me.events = function () {
        // For debugging selections uncommment the line below
        // qlik.app.getObject('CurrentSelections', 'CurrentSelections');
    }

    me.boot();
};
controller.$inject = ['$scope', '$rootScope', '$location', '$injector', 'qlik', 'api', 'app'];
angular.module('controller.sales', [])
    .controller('controller.sales', controller);
