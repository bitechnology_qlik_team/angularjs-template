var prefix = window.location.pathname.substr(0, window.location.pathname.toLowerCase().lastIndexOf("/extensions") + 1);
const config = {
    /*local windows*/
    host: 'berkayaskin',
    prefix: prefix,
    port: window.location.port,
    isSecure: window.location.protocol + '//'
}

require.config({
    /*baseUrl: window.location.protocol + "//" + window.location.host + prefix + "resources",*/
    baseUrl: (config.isSecure)
        + config.host
        + (config.port ? ":" + config.port : "")
        + config.prefix + "resources",
    paths: {
        'jquery': prefix + "extensions/angularjs-template/src/assets/js/core/jquery.min",
        'tether': prefix + "extensions/angularjs-template/src/js/vendor/tether.min", // for bootstrap 4 to work
        'bootstrap': prefix + "extensions/angularjs-template/src/assets/js/core/bootstrap.bundle.min",
        'ui.router': prefix + "extensions/angularjs-template/src/js/vendor/angular-ui-router.min",
        /*controllers*/
        'homeController': prefix + "extensions/angularjs-template/src/js/controllers/home",
        'salesController': prefix + "extensions/angularjs-template/src/js/controllers/sales",
        /*directives*/
        'dropdownDirective': prefix + "extensions/angularjs-template/src/js/directives/dropDown/dropdown",
        'getObjectDirective': prefix + "extensions/angularjs-template/src/js/directives/getObject/getObject",
        'getSelectionObjectDirective': prefix + "extensions/angularjs-template/src/js/directives/getSelectionObject/getSelectionObject",
        'kpiDirective': prefix + "extensions/angularjs-template/src/js/directives/kpi/kpi",
        /*services*/
        'apiServices': prefix + "extensions/angularjs-template/src/js/services/api",
        'appServices': prefix + "extensions/angularjs-template/src/js/services/app",
        'qlikServices': prefix + "extensions/angularjs-template/src/js/services/qlik",
        /*routes*/
        'routes': prefix + "extensions/angularjs-template/src/js/lib/routes",
        /*custom*/
        'custom': prefix + "extensions/angularjs-template/src/assets/js/custom"
    },
    shim: {
        "bootstrap": ['jquery', 'tether'],
        "perfectScrollbar": ['bootstrap'],
    },
});

require([
    'js/qlik',
    'angular',
    'jquery',
    'bootstrap',
    'ui.router',
    'routes',
    'homeController',
    'salesController',
    'apiServices',
    'appServices',
    'qlikServices',
    'dropdownDirective',
    'getObjectDirective',
    'getSelectionObjectDirective',
    'kpiDirective',
    'custom',
], function (qlik, angular, $) {
    window.qlik = qlik;
    qlik.setOnError( function ( error ) {
        if (!angular.isUndefined(error) && error.code === 16) {
            console.log('qlik.setOnError, error.code===16 : ', error.message);
        } else {
            console.log('qlik.setOnError: ', error.message);
        }
    } );

    angular.module('myApp', [
        'ui.router',
        'routes',
        'controller.home',
        'controller.sales',
        'directive.getObject',
        'directive.dropDown',
        'directive.getSelectionObject',
        'directive.kpi',
        'service.app',
        'service.api',
        'service.qlik',
    ])
    $(document).ready(function() {
        angular.bootstrap(document, ["myApp", "qlik-angular"]);

    });
});

define([
    'require',
    'angular',
    'tether',
], function (require, angular, Tether) {
    'use strict';
    window.Tether = Tether; // For Bootstrap 4 to work
});
