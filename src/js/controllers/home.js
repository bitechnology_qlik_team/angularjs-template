'use strict';

var controller = function ($scope, $rootScope, $location, $injector, qlik, api, app) {

    var me = {};

    me.init = function () {
        $rootScope.page = 'home';
    }

    me.boot = function () {
        me.init();
        me.events();
        app.log(`${$rootScope.page} loaded: `, 'Success!');
        app.log($rootScope.message);
    };

    me.events = function () {
        // For debugging selections uncommment the line below
        // qlik.app.getObject('CurrentSelections', 'CurrentSelections');
    }

    me.boot();
};
controller.$inject = ['$scope', '$rootScope', '$location', '$injector', 'qlik', 'api', 'app'], angular.module('controller.home', []).controller('controller.home', controller);
