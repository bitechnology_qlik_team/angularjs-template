'use strict';

angular.module('service.qlik', [])
.service('qlik', function ($q, $rootScope) {
	var me = this;

	me.version = '1.0.0';

	me.openModels = [];

	me.environment = {
		/*dev*/
		staging: {
			host: 'berkayaskin',
			/*host: '192.168.1.101',*/
			prefix: '',
			port: 443,
			isSecure: true,
			id: '65ef26bf-9b3e-46b1-a842-03c74a1bd24d'
		},
	};
	me.config = me.environment.staging;

	me.openApp = function () {
		var deferred = $q.defer();
		me.app = qlik.openApp(me.config.id, me.config);
		deferred.resolve(true);
		return deferred.promise;
	}

	me.openApp();

	me.getUserName = function () {
		var deferred = $q.defer();
		try {
			qlik.getGlobal(me.config).getAuthenticatedUser(function(reply){
				var userId = reply.qReturn.match(/UserId=([^\s]+)/g)[0].replace("UserId=","");
				var domTarget = document.getElementsByClassName("profile-name")[0].getElementsByTagName('span')[0];
				domTarget.innerHTML = userId;
			});
		} catch (e) {
			console.log(e)
		}

		deferred.resolve(true);
		return deferred.promise;
	}

	me.getUserName();

	$rootScope.export = function (id) {
		me.app.getObject(id).then(model => qlik.table(model).exportData({download: true}));
	}


	/*utility.log('App Service Loaded: ', me.version);*/
});
