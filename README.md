## [AngularJS Mashup Template](https://bitbucket.org/bitechnology_qlik_team/angularjs-template/src/master/)

#### How to run the project?
- Find the qlik.js and main.js files. Change the qlik config based on your hostname and appID.
- Change the name of application with the same name of angularjs-template.qext and the name fields inside of qext and package.json file.
- Zip the file and import your Qlik Sense Server. Open the https://hostname/extensions/project-name/index.html adress. Router will take you to the home route.
##### Changing css
- The project is created using scss. In order to change the css, you will have to install npm packages using <code>npm-install</code>. After that, you should command <code>npm watch:sass</code> to compile all the scss files into a styles.css file. 
##### Adding pages, controlling routes
- You can control the routes in routes.js file. We are going to be using ui-router library. The project has two pages as boilerplate. If you want to add a page, you should add it to routes.js and sidebar.html. If the page has its own controller, add it to controllers and require it in main.js using require.js
##### Get object
- Find the object id you would like to add the project. In the home.html and sales.html, you will have commented out get-object directives. Write the object id in qvid attribute.
##### Directives
- You will also have dropDown, getSelectionObject and kpi directives ready to use.
##### Services
- openApp and export functions are inside qlik.js service. Also you will have some built in api methods inside api.js service such as getHyperCube, getObject, createList, getList etc.
##### Deployment
- Run <code>npm run build:css</code>.This code will compress, use prefixes, and compress main stylesheet file.
- Don't forget to remove the node_modules and package-lock.json files. Otherwise, project size will be enormous due to npm packages inside node_modules.